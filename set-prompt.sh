################################################################################
# Filename: ~/bin/shared/include/set-prompt.sh                                 #
# Created:  2017-03-30                                                         #
# Modified: 2017-11-26: Refactored All The Things to ``./prompt/*.inc.sh``     #
#           2017-11-25: refactored: todo etc to ``todo.inc.sh``,               #
#               git etc to ``git.inc.sh``                                      #
#           2017-11-24: bugfix: now handles no ``todo.sh`` gracefully          #
#           2017-11-23: bugfix: now handles no ``git`` gracefully              #
#           2017-11-18: refactored todo, due, issue                            #
#           2017-11-12: bugfix: formatting glitch at prompt, $PS1_eol to fix   #
#           2017-10-19: show number of open issues (~/Issues/Open)             #
#           2017-10-09: remove '-2' todo.txt bugfix                            #
#           2017-10-06: todo reliant on ~/todo.txt again.                      #
#               (execs faster when using symlink and not using todo.sh)        #
#           2017-10-05: Cygwin added, working on Cygwin, not tested on macOS.  #
#           2017-10-03: todo not reliant on ~/todo.txt;                        #
#               OSX-battery code refactored to not break non-OSX;              #
#               preparing for Cygwin.                                          #
# Author:   john@webarch.coop                                                  #
# Purpose:  Configure the Bash prompt, PS1.                                    #
################################################################################

export PS1_success="✔"
export PS1_failure="✘"
export PS1_clock=" ◴ " # ◴◵◶◷⌚
export PS1_lineabove="⮑ "        # "Return-right"
export PS1_sepicon="✭"  #"䷰"  #"𐤛 " #"✪"   #"✵"  #"●"  # ★
export PS1_todo="☑"
export PS1_issues="🐛"
export PS1_pwr_ac="🔌 "
export PS1_pwr_batt="🔋 "
export PS1_directory="📂 "
export PS1_welcome="☺"

# Set-up solarized colours (including choosing light or dark theme)
. ~/bin/shared/include/prompt/solarized.inc.sh

export PS1_sep="${PS1_fggreen}${PS1_sepicon} ${PS1_fg}"

# get_display_previous_cmd
. ~/bin/shared/include/prompt/previous.inc.sh

# get_display_time
. ~/bin/shared/include/prompt/time.inc.sh

# get_display_host
. ~/bin/shared/include/prompt/host.inc.sh

# get_display_pwd
. ~/bin/shared/include/prompt/pwd.inc.sh

# get_display_todo_summary: todo, due, due7, issues
#. ~/bin/shared/include/prompt/todo.inc.sh

# get_display_power
. ~/bin/shared/include/prompt/power.inc.sh

PROMPT_COMMAND=__prompt_command

__prompt_command()
{
    local EXIT="$?" # Do this first! Beyond here, '$?' will refer to commands
                    # executed within this function.

    PS1=""

    # Line 1: return status, last command number, time, hostname, current directory (including git status), todo, power
    if [ "$PS1_starting" != "" ];
    then
        PS1+="$(get_display_previous_cmd 1)"
        PS1_starting=""
    else
        PS1+="$(get_display_previous_cmd 0)"
    fi
    PS1+="$(get_display_time)$(get_display_host)$(get_display_pwd)$(get_display_power)"
    # $(get_display_todo_summary)
    PS1+="${PS1_eol}\n"
    # Line 2: current command number, prompt
    PS1+="${PS1_fg2}!${PS1_fg}\! \$ ${PS1_eol}"
}
