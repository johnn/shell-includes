################################################################################
# Filename: ~/bin/shared/include/prompt/git.inc.sh                             #
# Created:  2017-11-25                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate ``git`` functions, primarily for set-prompt.sh         #
################################################################################

[ -f /usr/local/git/contrib/completion/git-prompt.sh ] && . /usr/local/git/contrib/completion/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM="auto"

get_display_git ()
{
    gitraw=""
    git=""
    if [ "$(declare -f __git_ps1)" ]; then
        gitraw="$(__git_ps1)"
        if [[ $(echo "$gitraw" | grep -E "[*]") ]]; then    # Tracked files UNSTAGED
            git="${PS1_fgred}"
        elif [[ $(echo "$gitraw" | grep -E "[+]") ]]; then  # Tracked files STAGED, NOT COMMITTED
            git="${PS1_fgorange}"
        elif [[ $(echo "$gitraw" | grep -E "[%]") ]]; then  # UNTRACKED files
            git="${PS1_fggreen}"
        else
            git="${PS1_fg}"
        fi
        git+="${gitraw} "
    fi
    echo "${git}"
}
