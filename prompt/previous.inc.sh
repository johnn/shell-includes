################################################################################
# Filename: ~/bin/shared/include/prompt/previous.inc.sh                        #
# Created:  2017-11-26                                                         #
# Modified: 2017-11-27: bugfix: display 'Welcome!' only once...                #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate previous command display, primarily for set-prompt.sh  #
################################################################################

## @param integer Non-zero indicates the terminal session has just started
get_display_previous_cmd ()
{
    local PS1=""
    local lasthistnum="$(history | tail -n 1 | awk '{ print $1 }')" ##TODO Check time this takes doesn't increase foolishly
    if [ $1 -eq 1 ];
    then
        PS1+="\n"
        PS1+="${PS1_bg2}${PS1_em}${PS1_welcome} ${PS1_fg}Welcome!"
    else
        if [ "${EXIT}" != "0" ]; then
            PS1+="${PS1_bg2}${PS1_fgred}${PS1_lineabove} ${PS1_failure} ${PS1_fg2}(${PS1_fgred}${EXIT}${PS1_fg2}) !${PS1_fgred}${lasthistnum}"
        else
            PS1+="${PS1_bg2}${PS1_fggreen}${PS1_lineabove} ${PS1_success} ${PS1_fg2}!${PS1_fggreen}${lasthistnum}"
        fi
    fi
    echo "${PS1}"
}
