################################################################################
# Filename: ~/bin/shared/include/prompt/pwd.inc.sh                             #
# Created:  2017-11-26                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate directory display, primarily for set-prompt.sh         #
# Depends:  ~/bin/shared/include/prompt/git.inc.sh                             #
################################################################################

# get_display_git
. ~/bin/shared/include/prompt/git.inc.sh

get_display_pwd ()
{
    curdir="${PS1_fg}${PS1_directory} \w$(get_display_git)"
    echo " ${PS1_sep} ${curdir}"
}
