################################################################################
# Filename: ~/bin/shared/include/prompt/todo.inc.sh                            #
# Created:  2017-11-12                                                         #
# Modified: 2018-03-04: Removed temp fix (unbroken now, brackets added)        #
#           2018-03-02: Temp fix for ``todo.sh due`` (broken: assumes Python2) #
#           2017-11-26: !todo, !issues now handled here                        #
#           2017-11-25: refactoring: ``get_display_todo_summary``              #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate ``todo.sh`` functions, primarily for set-prompt.sh     #
################################################################################

if [ "$(which todo.sh 2> /dev/null)" ];
then
    PS1_TODO_OK=1
else
    PS1_TODO_OK=0
fi

if [ -d ~/Issues ] && [ -d ~/Issues/Open ];
then
    PS1_ISSUES_OK=1
else
    PS1_ISSUES_OK=0
fi

## @return       Total number of todo items in todo.txt
get_number_of_todos ()
{
        echo $( cat ~/todo.txt|wc -l )
}

## @param        integer Optional number of days into the future
## @return       Number of todo items in todo.txt with a 'due:' key-value pair, where the value (YYYY-MM-DD) is on or before today + $1 days
get_number_of_todos_due ()
{
        echo $( todo.sh -p due $1 | grep -E '^[[:digit:]]+' | wc -l )
}

## @return      Number of open issues (files in ~/Issues/Open)
get_number_of_issues ()
{
        issues=""
        [ -d ~/Issues/Open ] && issues="$(printf "%i" $(\ls -1 ~/Issues/Open|wc -l))"
        echo ${issues}
}

if [ "${PS1_TODO_OK}" -eq 0 ] && [ "${PS1_ISSUES_OK}" -eq 0 ];
then
    get_display_todo_summary ()
    {
        echo ""
    }
else
    ## @Return  Textual, coloured summary of number of todos and issues
    get_display_todo_summary ()
    {
        numtodos="$(get_number_of_todos)"
        numdue="$(get_number_of_todos_due)"
        numdue7="$(get_number_of_todos_due 7)"
        numissues="$(get_number_of_issues)"

        todo=""
        due=""
        issues=""

        if [ "${PS1_TODO_OK}" -ne 0 ];
        then
            todo=" ${PS1_todo} ${PS1_fg2}t:"
            if [ "${numtodos}" -eq 0 ]; then
                todo+="${PS1_fg}"
            else
                todo+="${PS1_fgorange}"
            fi
            todo+="${numtodos}"

            due=" ${PS1_fg2}d:"
            if [ "${numdue}" -eq 0 ]; then
                due+="${PS1_fg}"
            else
                due+="${PS1_fgorange}"
            fi
            due+="${numdue} ${PS1_fg2}d7:"
            if [ "${numdue7}" -eq 0 ]; then
                due+="${PS1_fg}"
            else
                due+="${PS1_fgorange}"
            fi
            due+="${numdue7}"
        fi

        if [ "${PS1_ISSUES_OK}" -ne 0 ];
        then
            issues=" ${PS1_fg2}i:"
            if [ "${numissues}" -eq 0 ]; then
                issues+="${PS1_fg}"
            else
                issues+="${PS1_fgorange}"
            fi
            issues+="${numissues}"
        fi

        display_todo=" ${PS1_sep}${todo}${due}${issues}"
        echo ${display_todo}
    }
fi
