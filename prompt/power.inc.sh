################################################################################
# Filename: ~/bin/shared/include/prompt/power.inc.sh                           #
# Created:  2017-11-26                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate AC/DC/charge display, primarily for set-prompt.sh      #
# To do:    Currently only useful on Darwin (macOS)                            #
################################################################################

get_display_power ()
{
    power=""
    if [[ "$(uname)" == "Darwin" ]]; then
        power=" ${PS1_sep} "
        # Some code pinched from: https://github.com/Goles/Battery/blob/master/battery
        if [ "$(pmset -g batt | grep -o 'AC Power')" ]; then
            power+="${PS1_pwr_ac}"
        else
            power+="${PS1_pwr_batt}"
        fi
        charge=$(pmset -g batt | grep -o '[0-9]*%' | tr -d '%')
        if [[ charge -lt 100 ]]; then
            if [[ charge -lt 40 ]]; then
                power+="${PS1_fgred}"
            else
                if [[ charge -lt 70 ]]; then
                    power+="${PS1_fgyellow}"
                else
                    power+="${PS1_fggreen}"
                fi
            fi
            power+="$charge%${PS1_fg}"
        fi
    fi
    echo "${power}"
}
