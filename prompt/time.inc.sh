################################################################################
# Filename: ~/bin/shared/include/prompt/time.inc.sh                            #
# Created:  2017-11-26                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate ``time`` display, primarily for set-prompt.sh          #
################################################################################

get_display_time ()
{
    echo "${PS1_em}${PS1_clock}${PS1_fg}\t"
}
