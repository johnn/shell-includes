################################################################################
# Filename: ~/bin/shared/include/prompt/solarized.inc.sh                       #
# Created:  2017-11-26                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate ``solarized`` set-up, primarily for set-prompt.sh      #
# URL:      http://ethanschoonover.com/solarized                               #
# Exports to environment:                                                      #
#           PS1_fgyellow,  PS1_fgorange, PS1_fgred  }                          #
#           PS1_fgmagenta, PS1_fgviolet, PS1_fgblue } colours                  #
#           PS1_fgcyan,    PS1_fggreen              }                          #
#           PS1_em         optional EMphasised content                         #
#           PS1_fg         ForeGround: primary content                         #
#           PS1_fg2        ForeGround2: secondary content                      #
#           PS1_bg         BackGround                                          #
#           PS1_bg2        BackGround2: background highlights                  #
#           PS1_eol        ``tput sgr0`` (turn off all attributes)             #
################################################################################

fgbase03="\[$(tput setaf 8)\]"
fgbase02="\[$(tput setaf 0)\]"
fgbase01="\[$(tput setaf 10)\]"
fgbase00="\[$(tput setaf 11)\]"
fgbase0="\[$(tput setaf 12)\]"
fgbase1="\[$(tput setaf 14)\]"
fgbase2="\[$(tput setaf 7)\]"
fgbase3="\[$(tput setaf 15)\]"
export PS1_fgyellow="\[$(tput setaf 136)\]"
export PS1_fgorange="\[$(tput setaf 166)\]"
export PS1_fgred="\[$(tput setaf 160)\]"
export PS1_fgmagenta="\[$(tput setaf 125)\]"
export PS1_fgviolet="\[$(tput setaf 61)\]"
export PS1_fgblue="\[$(tput setaf 33)\]"
export PS1_fgcyan="\[$(tput setaf 37)\]"
export PS1_fggreen="\[$(tput setaf 64)\]"
bgbase03="\[$(tput setab 8)\]"
bgbase02="\[$(tput setab 0)\]"
bgbase01="\[$(tput setab 10)\]"
bgbase00="\[$(tput setab 11)\]"
bgbase0="\[$(tput setab 12)\]"
bgbase1="\[$(tput setab 14)\]"
bgbase2="\[$(tput setab 7)\]"
bgbase3="\[$(tput setab 15)\]"
bgyellow="\[$(tput setab 136)\]"
bgorange="\[$(tput setab 166)\]"
bgred="\[$(tput setab 160)\]"
bgmagenta="\[$(tput setab 125)\]"
bgviolet="\[$(tput setab 61)\]"
bgblue="\[$(tput setab 33)\]"
bgcyan="\[$(tput setab 37)\]"
bggreen="\[$(tput setab 64)\]"
export PS1_eol="\[$(tput el)$(tput sgr0)\]"

# Solarized-dark:
setup_dark ()
{
    export PS1_em="$fgbase1"    # emphasized
    export PS1_fg="$fgbase0"    # primary foreground content
    export PS1_fg2="$fgbase01"  # secondary foreground content

    export PS1_bg2="$bgbase02"  # background highlights
    export PS1_bg="$bgbase03"   # background
}

# Solarized-light:
setup_light ()
{
    export PS1_em="$fgbase01"   # emphasized
    export PS1_fg="$fgbase00"   # primary foreground content
    export PS1_fg2="$fgbase1"   # secondary foreground content

    export PS1_bg2="$bgbase2"   # background highlights
    export PS1_bg="$bgbase3"    # background
}

theme="solarized-dark"
if [ "${PROMPT_THEME}" != "" ]; then
    theme="${PROMPT_THEME}"
fi

if [ "$theme" = "solarized-dark" ]; then
    setup_dark
else
    setup_light
fi
