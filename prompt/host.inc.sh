################################################################################
# Filename: ~/bin/shared/include/prompt/host.inc.sh                            #
# Created:  2017-11-26                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate ``host`` display, primarily for set-prompt.sh          #
# To do:    Test with other OSs, specifically BSDs.                            #
# To do:    Hostname? Shouldn't it be done just once? Check with SSH.          #
################################################################################

# OS
if [[ "$(uname)" == "Darwin" ]]; then
    export PS1_os="⌘ "
elif [[ "$(uname|cut -c1-6)" == "CYGWIN" ]]; then
    export PS1_os="⊞ "
else	# Default to Linux
    export PS1_os="🐧 "
fi

# Session type
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    export SESSION_TYPE=remote/ssh
else
  :
# Broken on Cygwin (and, if I'm honest, probably on GNU/non-BSD):
#    case $(ps -o comm= -p $PPID) in
#        sshd|*/sshd) export SESSION_TYPE=remote/ssh;;
#    esac
fi

get_display_host ()
{
    hostname="${PS1_em}${PS1_os}"
    if [[ "${SESSION_TYPE}" == "remote/ssh" ]]; then
        hostname+="SSH: ${PS1_fg}"
    else
        hostname+="${PS1_fg2}"
    fi
    if [ -z "$PROMPT_HIDE_HOSTNAME" ]; then
        hostname+="\h"
    else
        hostname+="HOSTNAME REDACTED"
    fi

    echo " ${PS1_sep} ${hostname}"
}
