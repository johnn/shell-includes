################################################################################
# Purpose:  Retry a command 3 times, with a delay between each attempt.
# Comments: INCLUDE!
# Author:	  john@webarch.coop
# Created:	2017-03-13
################################################################################

retry ()
{
    local n=1
    local max=3
    local delay=10
    cmd="$@"
    while true; do
        log_debug "About to attempt to run command \"${cmd}\" (attempt $n/$max)."
        "$@" && break || {
            if [[ $n -lt $max ]]; then
                log_warn "Attempt ($n/$max) to run command \"${cmd}\" failed. Retrying..."
                ((n++))
                sleep $delay;
            else
                log_error_and_exit "Command \"${cmd}\" has failed after $n attempts." "1"
            fi
        }
    done
}
