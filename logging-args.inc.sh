################################################################################
# logging-args.inc.sh
# Logging command-line arguments include. Source this file.
################################################################################
# Author:   john@webarch.coop
# Created:  2017-02-23
# Modified: 2017-03-19: Updated usage.
# Depends:  logging.inc.sh
################################################################################

usage ()
{
    printf 'Usage: %s [OPTIONS]\n\n' "${shortname}"
    printf 'Options:\n'
    print_logging_usage
}

while :; do
    case $1 in
        -1|--log-level=error)
            verbose=$ERROR
            ;;
        -2|--log-level=warn)
            verbose=$WARN
            ;;
        -3|--log-level=info)
            verbose=$INFO
            ;;
        -4|--log-level=debug)
            verbose=$DEBUG
            ;;
        -5|--log-level=trace)
            verbose=$TRACE
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        --)     # End of all options.
            shift
            break
            ;;
        -?*)
            log_warn "Unknown option (ignored): $1"
            ;;
        *)      # Default case: If no more options then break out of the loop.
            break
    esac
    shift
done
