################################################################################
# Filename: ~/bin/shared/include/bash_profile/history-monolithic.inc.sh        #
# Created:  2017-11-12                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate monolithic history functionality                       #
# Caveat:   Not formally tested on GNU/Cygwin, but pretty certain it worked    #
################################################################################

# jn, 2016-11-04, http://mywiki.wooledge.org/BashFAQ/088
# Update (append to) .bash_history after each command:
HISTFILESIZE=400000000
HISTSIZE=10000
PROMPT_COMMAND="${PROMPT_COMMAND:-:} ; history -a"
shopt -s histappend
