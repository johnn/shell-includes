################################################################################
# Filename: ~/bin/shared/include/bash_profile/history-multiple-sessions.inc.sh #
# Created:  2017-11-12                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate multi-session history functionality                    #
# To do:    Test on GNU/Cygwin                                                 #
################################################################################

# john@webarch.coop, added, 2017-11-09:
# See: https://askubuntu.com/questions/80371/bash-history-handling-with-multiple-terminals
[[ -d ~/.history ]] || mkdir ~/.history # ``--mode=0700``: a GNUism?
[[ -d ~/.history ]] && chmod 0700 ~/.history
#HISTFILE=~/.history/history.$$ # Process ID of the shell
# Make history files sort chronologically when using standard (A-Z) sorting:
HISTFILE=~/.history/history.$(date "+%Y%m%d%H%M%S%z")_$$        # YYYYMMDDHHMMSS+TZOS_$$ (TZOF: timezone offset, e.g. +0100 for BST)
# close any old history file by zeroing HISTFILESIZE
HISTFILESIZE=0
# then set HISTFILESIZE to a large value
HISTFILESIZE=4096
HISTSIZE=4096
shopt -s histappend     # This is necessary, don't delete it
