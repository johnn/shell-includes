# Shell includes

Shell files from `~/bin/shared/include`. These should work on multiple platforms (Linux, macOS, etc) and aim to avoid bash-isms, GNU-isms, etc.

# Overview

## Useful links

# Cheatsheet

# License

Copyright (c) 2021 John Niven

Licensed under the GPL, v3.
