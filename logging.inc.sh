################################################################################
# logging.inc.sh
# Logging include. Source this file.
################################################################################
# Author:   john@webarch.coop
# Created:  2017-02-10
# Modified: 2017-11-13: Bugfix: redirect ``gdate`` noise to ``/dev/null``
#           2017-11-12: Use ``gdate`` on macOS (no ``%N`` on BSD ``date``)
#           2017-11-06: ``log_debug``, ``log_trace``: nanosecond precision
#           2017-04-02: Bug-fix: notify conditional on user != root.
#           2017-03-28: Use local time (and timezone offset) instead of UTC
#           2017-02-13: Changed log-level constants' values.
# Todo:
#         * make notifications optional/configurable/set by param/whatevs
#           (rationale: useful for infrequent stuff eg. mount, useless for
#           frequent stuff, eg. path changes)
# Caveats:
# 	      * BSD ``date`` only does second-presion. For nanosecond-precision
#           use GNU ``gdate`` (``brew install coreutils``)
#         * ``realpath`` is a GNUism  (``brew install coreutils``)
################################################################################

ERROR=1 # Default
WARN=2
INFO=3
DEBUG=4
TRACE=5

longname=$0
[ -f /usr/bin/realpath ] && longname=$(/usr/bin/realpath $0) # Non-portable GNU-ism. On macOS: ``brew install coreutils``
shortname=$(/usr/bin/basename "${longname}")
verbose=$ERROR

[ $(which gdate 2> /dev/null) ] && DATECMD="$(which gdate)" || DATECMD="$(which date)"

print_logging_usage ()
{
  printf '  -1, --log-level=error\n'
  printf '\t\tlog errors to stdout (NB. errors are always logged to stderr)\n'
  printf '  -2, --log-level=warn\n'
  printf '\t\tlog warnings (and errors) to stdout (NB. warnings are always logged to stderr)\n'
  printf '  -3, --log-level=info\n'
  printf '\t\tlog info (and lower log-level messages) to stdout\n'
  printf '  -4, --log-level=debug\n'
  printf '\t\tlog debug (and lower log-level messages) to stdout\n'
  printf '  -5, --log-level=trace\n'
  printf '\t\tlog trace (and lower log-level messages) to stdout\n'
  printf '  -h, --help\tdisplay this help and exit\n'
}

# Write start message to stdout
# @param    string  Optional start message
log_start ()
{
    if [ -z "$1" ]; then
        msg="${longname} started."
    else
        msg="${longname} started: $1"
    fi
    log_trace "$msg"
}

# Write success message to stdout, and then exit (with return code 0)
# @param    string  Optional success message
log_success_and_exit ()
{
    if [ -z "$1" ]; then
        msg="${longname} completed successfully."
    else
        msg="${longname} completed successfully: $1"
    fi
    log_info "$msg"
    #if [ "$(id -u)" != "0" ]; then  # Don't notify if running as root.
    #    [ -f /usr/bin/osascript ] && osascript -e "display notification \"$msg\" with title \"Finished: $shortname\" sound name \"Hero\""
    #fi
    exit 0
}

# Write error message to stdout, write error message and result to stderr, exit (with return code >0)
# @param    string  Optional error message
# @param    integer Optional exit code (1-255, defaults to 1)
# @param    string  Optional error text
log_error_and_exit ()
{
    if [ -z "$1" ]; then
        msg="${longname} encountered an unspecified error. Exiting."
    else
        msg=$1
    fi
    if [ -z "$3" ]; then
        printf '%s %s (%s) ERROR: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "${longname}" "$msg" >&2   # stderr
    else
        printf '%s %s (%s) ERROR: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "${longname}" "$msg" >&2   # stderr
        if [ -z "$3" ]; then
            printf '%s\n' "$3" >&2                                                                                  # stderr
        fi
        printf '\n\'
    fi
    if [ $verbose -ge $ERROR ]; then
        printf '%s %s (%s) ERROR: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "${longname}" "$msg"       # stdout
        if [ "$(id -u)" != "0" ]; then  # Don't notify if running as root.
            [ -f /usr/bin/osascript ] && osascript -e "display notification \"$msg\" with title \"Error: $shortname\" sound name \"Funk\""
        fi
    fi
    if [ -z "$2" ]; then
        exit 1
    else
        exit "$2"
    fi
}

# Write warning message to stderr (and, optionally, to stdout).
# @param    string  Optional warn message
log_warn ()
{
    if [ -z "$1" ]; then
        msg="${longname} encountered an unspecified but recoverable error. Continuing."
    else
        msg=$1
    fi
    printf '%s %s WARN: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "$msg" >&2   # stderr
    if [ $verbose -ge $WARN ]; then
        printf '%s %s WARN: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "$msg"   # stdout
    fi
}

# Optionally write info message to stdout
# @param    string  Optional info message
log_info ()
{
    if [ $verbose -ge $INFO ]; then
        if [ -z "$1" ]; then
            msg="log_info: information not specified."
        else
            msg=$1
        fi
        printf '%s %s INFO: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "$msg"
    fi
}

# Optionally write debug message to stdout
# @param    string  Optional debug message
log_debug ()
{
    if [ $verbose -ge $DEBUG ]; then
        if [ -z "$1" ]; then
            msg="log_info: debug information not specified."
        else
            msg=$1
        fi
        printf '%s %s DEBUG: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "$msg"
    fi
}

# Optionally write trace message to stdout
# @param    string  Optional trace message
log_trace ()
{
    if [ $verbose -ge $TRACE ]; then
        if [ -z "$1" ]; then
            msg="log_info: trace information not specified."
        else
            msg=$1
        fi
        printf '%s %s TRACE: %s\n' "$(${DATECMD} "+%Y-%m-%d %H:%M:%S.%N %z")" "${shortname}" "$msg"
    fi
}
