################################################################################
# Filename: ~/bin/shared/include/prompt/time.inc.sh                            #
# Created:  2017-10-19                                                         #
# Modified:                                                                    #
# Author:   john@webarch.coop                                                  #
# Purpose:  Encapsulate ``time`` display, primarily for set-prompt.sh          #
################################################################################

alias issues='~/bin/shared/scripts/issues-list-open.sh'
alias i=issues

# TO DO:
# * Test this on Cygwin/GNU.
# * Refactor into ~/bin/shared/scripts/issues.sh (or whatevs)
