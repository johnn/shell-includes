################################################################################
# Filename: ~/bin/shared/include/function-greph.sh                             #
# Created:  2016-11-22                                                         #
# Modified: 2017-11-24: Bugfix: test ``~/.history/`` exists                    #
#           2017-11-23: ``shopt -s nullglob``                                  #
#           2021-04-07: Updated email for git.coop                             #
# Author:   john@webarch.coop                                                  #
# Purpose:  GREP History: Search current and previous sessions' history for    #
#           the specified term                                                 #
################################################################################

greph ()
{
    : Search history for TERM.
    if [ -z "$*" ]; then
        echo "Search history (current and past sessions) for TERM."
        echo "Usage: greph TERM"
    else
        # Matches from previous sessions:
        if [ -d ~/.history ]; then
            shopt -s nullglob
            cat ~/.history/history.* | nl | \
            grep "$*" --color=always | egrep -v 'greph|gref' --colour=always | \
            LC_ALL='C' sort -r -k 2 | uniq -f 1 | sort | \
            sed "s/^[[:space:]]*[[:digit:]]*[[:space:]]*/$(printf '\t')/"
            shopt -u nullglob
        fi
        # Matches from current session (starts with '!<command num>'):
        history | \
        grep "$*" --color=always | egrep -v 'greph|gref' --color=always | \
        LC_ALL='C' sort -r -k 2 | uniq -f 1 | sort -n | \
        sed -E "s/^[[:space:]]*([[:digit:]]*)[[:space:]]*/\!\1$(printf '\t')/g"
    fi
}
alias gref="greph"
